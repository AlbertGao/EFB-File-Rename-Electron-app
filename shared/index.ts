export namespace setFolder {
  export interface Req {
    type: "set-folder";
    id: string;
    path: string;
  }

  export interface Res {
    type: "set-folder";
    data: {
      ready: boolean;
      main: number;
      dep: number;
      mainDb: boolean;
      depDb: boolean;
    };
  }
}

export namespace startTask {
  export interface Req {
    type: "start-task";
    id: string;
  }
  export interface Res {
    type: "start-task";
    data: {
      step: number;
    };
  }
}
