import Path from "path";
import { __projectDir as proj } from "albert.gao/node";
export const filesToCopy: string[] = [
  Path.resolve(proj, ".vscode"),
  Path.resolve(proj, "dev-tools"),
  Path.resolve(proj, "shared"),
  Path.resolve(proj, "docker-debian"),
  Path.resolve(proj, "vue/assets"),
  Path.resolve(proj, "vue/components"),
  Path.resolve(proj, "vue/pages"),
  Path.resolve(proj, "vue/router"),
  Path.resolve(proj, "vue/stores"),
  Path.resolve(proj, "vue/entry-client.ts"),
  Path.resolve(proj, "vue/index.html"),
  Path.resolve(proj, "vue/tsconfig.json"),
  Path.resolve(proj, ".dockerignore"),
  Path.resolve(proj, ".eslintrc.js"),
  Path.resolve(proj, ".gitignore"),
  Path.resolve(proj, "Dockerfile"),
  Path.resolve(proj, "ecosystem.config.js"),
  Path.resolve(proj, "electron-builder.js"),
  // Path.resolve(proj, "electron-mirror-server.ts"),
  Path.resolve(proj, "package.json"),
  Path.resolve(proj, "tsconfig.json"),
  Path.resolve(proj, "vite.config.ts"),
  // 在最后再挪到新的目录
  Path.resolve(proj, "backend"),
].map((e) => Path.relative(proj, e));

export default filesToCopy;
