import { defineStore, storeToRefs } from "pinia";
import { nanoid } from "nanoid";
import { getWsAddress, wsSend, wsOn } from "albert.gao-es/browser";
import { ElNotification } from "element-plus";
import { watchEffect } from "vue";
import * as A from "~/shared";
// useStore could be anything like useUser, useCart
// the first argument is a unique id of the store across your application
let path = "";
let id: string = nanoid();

let readyState: A.setFolder.Res["data"] = {
  ready: false,
  main: 0,
  dep: 0,
  mainDb: false,
  depDb: false,
};

export const ws = new WebSocket(getWsAddress());
export const useStore = defineStore("test", {
  // other options...
  state: () => ({
    path,
    id,
    step: 0,
    readyState,
  }),
  getters: {
    isInElectronBrowser() {
      try {
        require("@electron/remote").getCurrentWindow();
        return true;
      } catch (error) {
        return false;
      }
    },
  },
  actions: {
    setFolder() {
      let { id, path } = this;
      let toSend: A.setFolder.Req = {
        type: "set-folder",
        path,
        id,
      };
      wsSend(toSend, ws);
    },
    startTask() {
      let { id } = this;
      let toSend: A.startTask.Req = {
        type: "start-task",
        id,
      };
      wsSend(toSend, ws);
    },
    init() {
      wsOn(
        [
          (data: A.setFolder.Res | A.startTask.Res) => {
            if (data.type === "set-folder") {
              this.readyState = data.data;
            } else if (data.type === "start-task") {
              this.step = data.data.step;
            }
          },
        ],
        ws
      );
    },
  },
});

export const store = useStore();
store.init();
export default storeToRefs(store);

watchEffect(() => {
  if (store.readyState.ready) {
    ElNotification({
      message: "文件夹选择正确",
      type: "success",
    });
  }
});
