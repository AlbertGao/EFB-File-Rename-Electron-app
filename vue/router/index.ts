import { createHistoryRouter } from "albert.gao-es/default/vue-router";
// @ts-ignore
export default createHistoryRouter(import.meta.glob("/pages/**/*.vue"));
