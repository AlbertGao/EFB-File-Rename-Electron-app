import App from "albert.gao-es/Default.vue";
import "element-plus/dist/index.css";
import { createPinia } from "pinia";
import { createApp } from "vue";
import router from "@/router";

(async () => {
  const app = createApp(App);
  app.use(createPinia());
  app.use(router);
  await router.isReady();
  app.mount("#app");
})();
