"需要在background.js中运行本文件";
// import { importWss, importApi, importWebworkers } from "@/server/imports";
import { expressWssServer } from "albert.gao";
import { autoImport } from "albert.gao/default/backend-auto-import";
import { vueServer } from "albert.gao/default/vue-production-server";

export interface Options {
  production: boolean;
  port: number;
  changePortWhenOccupied?: boolean;
}

export default async function ({
  production = true,
  port = 8080,
  changePortWhenOccupied = true, // 在production模式下，不自动换port
}: Options) {
  production
    ? console.log("Server running in production mode")
    : console.log("Server running in development mode");
  return await expressWssServer({
    port,
    changePortWhenOccupied, // 在production模式下，不换port
    killPortWhenOccupied: production,
    setup: [
      autoImport,
      async ({ app }) => app.use(await vueServer(production)),
    ],
  });
}
