import { getIps } from "albert.gao/node";
import { Router } from "express";

const router = Router();

/** 返回id的100倍 */
router.use("/api/test/:id", function (req, res) {
  const {
    params: { id },
  } = req;
  res.json({ id: +id * 100 });
});

/** 返回所有ip地址 */
router.use("/api/get-ips", (_req, res) => {
  res.json(getIps());
});

export default router;
