import { Router } from "express";
import open from "open";

const router = Router();

/** 新建一个project */
router.use("/api/open-link", async (req, res) => {
  let {
    body: { link },
  } = req;
  try {
    open(link);
  } catch (error) {
    open(link);
  }
  res.status(200).end();
});

export default router;
