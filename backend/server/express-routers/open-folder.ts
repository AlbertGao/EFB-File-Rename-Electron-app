import { Router } from "express";
import open from "open";

const router = Router();

/** 新建一个project */
router.use("/api/open-project-folder", async (req, res) => {
  let {
    body: { path },
  } = req;
  try {
    open(path, {
      app: {
        name: "code",
      },
    });
  } catch (error) {
    open(path);
  }
  res.status(200).end();
});

export default router;
