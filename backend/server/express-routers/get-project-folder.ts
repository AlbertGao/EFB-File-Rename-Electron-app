import { dir } from "albert.gao";
import { Router } from "express";

const router = Router();

/** 返回当前project的上一级目录 */
router.use("/api/project-folder", (_req, res) => {
  res.json(dir("../", "dir"));
});

export default router;
