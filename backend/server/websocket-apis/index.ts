import type { WsHandler } from "albert.gao/node/network";
import * as A from "~/shared";
import * as store from "@/store";
import { start } from "@/utils";
let handle: WsHandler = {
  e: "message",
  func: ({ json, reply, client }) => {
    let obj: A.setFolder.Req | A.startTask.Req = json;
    try {
      if (obj.type === "set-folder") {
        let { id, path } = obj;
        if (!store.tasks.value[id]) {
          store.addTask(id, path);
          client.on("close", () => store.removeTask(id));
        } else {
          store.tasks.value[id].refresh(path);
        }
        let { task } = store.tasks.value[id];
        let dataToReply: A.setFolder.Res = {
          type: "set-folder",
          data: {
            ready: task.ready,
            main: task.mainPdfCount,
            dep: task.depPdfCount,
            mainDb: task.manualPlusDbExist,
            depDb: task.manualDepDbExist,
          },
        };
        reply(dataToReply);
      } else if (obj.type === "start-task") {
        let {
          task: { dirPath },
        } = store.tasks.value[obj.id];
        start(dirPath, (step) => {
          let dataToReply: A.startTask.Res = {
            type: "start-task",
            data: { step },
          };
          reply(dataToReply);
        });
        // task.task.start((step) => {
        //   let dataToReply: A.startTask.Res = {
        //     type: "start-task",
        //     data: { step },
        //   };
        //   reply(dataToReply);
        // });
      }
    } catch (error) {
      console.warn(error, json);
    }
  },
};
export default handle;
