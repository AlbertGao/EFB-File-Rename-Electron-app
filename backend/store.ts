import Task from "./utils";
import { ref } from "vue";

export interface Tasks {
  [key: string]: Task;
}

export const tasks = ref<Tasks>({});

export function addTask(id: string, path: string) {
  tasks.value[id] = new Task(path);
}

export function removeTask(id: string) {
  delete tasks.value[id];
}
