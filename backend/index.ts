("本脚本仅用于nodejs启动服务器，不适用于electron");
import { default as start } from "./server";
const args = process.argv;

/**
 * 获取命令参数
 * @returns 获取道德命令参数对象
 */
function getOptions(): { production: boolean; port: number } {
  const obj = { production: false, port: 80 };
  for (const item of args) {
    if (/-production/i.test(item)) {
      obj.production = true;
    }
    if (/-ports=\[\d+,\d+\]/.test(item)) {
      const {
        //@ts-ignore
        groups: { port },
      } = /-ports=\[(?<port>\d+),(?<httpsPort>\d+)\]/i.exec(item);
      Object.assign(obj, { port });
    }
  }
  console.log(obj);
  return obj;
}

start(getOptions());
