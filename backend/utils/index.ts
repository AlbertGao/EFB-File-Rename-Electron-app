import { nanoid } from "nanoid";
import sqlite3 from "better-sqlite3";
import Fs from "fs-extra";
import klawSync from "klaw-sync";
import _ from "lodash";
import Path from "path";
import { ref } from "vue";
import CP from "child_process";
// import { dir } from "albert.gao";

/**
 * 部门手册
 */
export namespace Dep {
  export interface Deptmanual_list {
    /** MD5 文件原有的名称，但是不带.pdf */
    MD5: string;
    /** 文件所在的文件夹 */
    ID: string;
    /** FILE_NAME 文件后来的名称，但是不带.pdf */
    FILE_NAME: string;
    /** 文件原有的名称(md5) + .pdf */
    md5Name: string;
    [key: string]: any;
  }
  export function get_deptmanual_list(db: sqlite3.Database): Deptmanual_list[] {
    return db
      .prepare("SELECT * FROM deptmanual_list")
      .all()
      .map((e) => ({ ...e, md5Name: e.MD5 + ".pdf" } as Deptmanual_list));
  }
  export interface Deptmanual2fleet {
    MANUAL2FLEET_ID: string;
    /** 相当于Deptmanual_list的ID, 也就是原文件夹名 */
    MANUALLIST_ID: string;
    MESSAGETYPEID: string;
    [key: string]: string;
  }
  /**
   * 目前用不上
   * @param db
   * @returns
   */
  export function get_deptmanual2fleet(
    db: sqlite3.Database
  ): Deptmanual2fleet[] {
    return db.prepare("SELECT * FROM deptmanual2fleet").all();
  }
  export interface Deptmanual2dept {
    MANUAL2DEPT_ID: string;
    /** 相当于Deptmanual_list的ID, 也就是原文件夹名 */
    MANUALLIST_ID: string;
    /** 相当于message_type中的MESSAGETYPEID */
    MESSAGETYPEID: string;
    [key: string]: string;
  }
  export function get_deptmanual2dept(db: sqlite3.Database): Deptmanual2dept[] {
    return db.prepare("SELECT * FROM deptmanual2dept").all();
    //   .map((e) => _.mapValues(e, (v) => v.trim()));
  }
  export interface Message_type {
    /** 同deptmanual2dept的MESSAGETYPEID */
    MESSAGETYPEID: string;
    /** 应该有的文件夹名(最后一级目录名称) */
    S_NAME: string;
    /** 其所属父文件夹 */
    PARENT_ID: string;
    [key: string]: string;
  }
  export function message_type(db: sqlite3.Database): Message_type[] {
    return db.prepare("SELECT * FROM message_type").all();
  }
  export namespace GetTasks {
    export interface Arg {
      depmanual_list: Deptmanual_list[];
      deptmanual2fleet: Deptmanual2fleet[];
      deptmanual2dept: Deptmanual2dept[];
      message_type: Message_type[];
    }
    export interface Result {
      /** 原目录名称 */
      fromDir: string;
      /** 原文件名 */
      fromFile: string;
      /** 翻译后目录名称 */
      toDir: string;
      /** 翻译后的文件名称 */
      toFile: string;
    }
  }
  /**
   * 根据id获取所有父文件夹
   * @param message_type message_type数据表
   * @param id depmanual2dept中的MESSAGETYPEID
   * @returns 父文件夹名称组成的数组
   */
  export function getParents(
    message_type: Message_type[],
    id: string
  ): string[] {
    const result: Message_type[] = [];
    let cur = _.cloneDeep(_.find(message_type, (e) => e.MESSAGETYPEID === id));
    while (cur) {
      result.unshift(cur);
      cur = _.cloneDeep(
        _.find(message_type, (e) => e.MESSAGETYPEID === cur?.PARENT_ID)
      );
    }
    return result.map((e) => e.S_NAME);
  }

  export function getTasks(arg: GetTasks.Arg): GetTasks.Result[] {
    return arg.depmanual_list.map(({ md5Name, ID, FILE_NAME }) => {
      let result: GetTasks.Result = {
        fromDir: ID,
        fromFile: md5Name,
        toDir: "unknown",
        toFile: FILE_NAME + ".pdf",
      };
      const { MESSAGETYPEID } = _.find(arg.deptmanual2dept, {
        MANUALLIST_ID: ID,
      }) || {
        MESSAGETYPEID: "unknown",
      };
      // const { S_NAME } = _.find(arg.message_type, { MESSAGETYPEID }) || {
      //   S_NAME: "unknown",
      // };
      // result.toDir = S_NAME;
      const parents = getParents(arg.message_type, MESSAGETYPEID);
      result.toDir = parents.join("/") || "unknown";
      return result;
    });
  }

  export function gather(filePath: string): GetTasks.Arg {
    const db = sqlite3(filePath);
    let result = {
      depmanual_list: get_deptmanual_list(db),
      deptmanual2fleet: get_deptmanual2fleet(db),
      deptmanual2dept: get_deptmanual2dept(db),
      message_type: message_type(db),
    };
    db.close();
    return result;
  }

  export function toTask(filePath: string): GetTasks.Result[] {
    return getTasks(gather(filePath));
  }

  export function figureOut(path: string, db: GetTasks.Result[]) {
    let fromFile = Path.basename(path);
    let fileDir = Path.dirname(path);
    let found = _.find(db, { fromFile });
    let toDir = found?.toDir;
    let toFile = found?.toFile;
    if (toDir && toFile) {
      let aftPath = Path.resolve(fileDir, toDir, toFile);
      Fs.moveSync(path, aftPath, { overwrite: true });
    }
  }

  export function exec(dirPath: string, db: GetTasks.Result[]) {
    for (let { path } of klawSync(dirPath, {
      nodir: true,
    }).filter((e) => !e.stats.isDirectory() && /\.pdf$/.test(e.path))) {
      figureOut(path, db);
    }
  }
}

/**
 * 主要手册
 */
export namespace Main {
  export interface Manual_list {
    /** MD5 文件原有的名称，但是不带.pdf */
    MD5: string;
    /** 文件重命名后应该所在的文件夹 */
    MANUALTYPE_NAME: string;
    /** FILE_NAME 文件后来的名称，不带.pdf */
    S_NAME: string;
    /** 修正有的不带.pdf结尾的文件名 */
    fileName: string;
  }
  export function get_manual_list(db: sqlite3.Database): Manual_list[] {
    return db.prepare("SELECT * FROM manual_list").all();
  }

  export function figureOut(path: string, db: Manual_list[]) {
    const fromFile = Path.basename(path);
    let fileDir = Path.dirname(path);
    let md5 = fromFile.replace(/\.pdf$/, "");
    let found = _.find(db, { MD5: md5 });
    if (found) {
      let aftPath = Path.resolve(
        fileDir,
        "../",
        found.MANUALTYPE_NAME,
        found.fileName
      );
      // debugger;
      try {
        Fs.moveSync(path, aftPath, { overwrite: true });
      } catch (error) {
        console.log({ path, aftPath });
      }
    } else {
      let aftPath = Path.resolve(fileDir, "../unknown", fromFile);
      Fs.moveSync(path, aftPath, { overwrite: true });
    }
  }

  export function gather(filePath: string): Manual_list[] {
    const db = sqlite3(filePath);
    let result = get_manual_list(db);
    db.close();
    return result.map((e) => {
      if (/.pdf$/i.test(e.S_NAME)) {
        e.fileName = e.S_NAME;
      } else {
        e.fileName = e.S_NAME + ".pdf";
      }
      return e;
    });
  }

  export function exec(dirPath: string, db: Manual_list[]) {
    for (let { path } of klawSync(dirPath, {
      nodir: true,
    }).filter((e) => Path.extname(e.path) === ".pdf")) {
      figureOut(path, db);
    }
  }
}

export class Task {
  /** manualPlus.db3 文件路径 */
  manualPlusDb: string;
  /** DepartmentManual.db3 文件路径 */
  manualDepDb: string;
  /** manualPlus.db3 是否存在 */
  manualPlusDbExist: boolean;
  /** manualDepDbExist.db3 是否存在 */
  manualDepDbExist: boolean;
  /** 主要手册路径 */
  mainPdfPath: string;
  /** 主要手册路径 + Manual/PDFFile */
  // mainPdfPathDeep: string;
  /** 主要手册数量 */
  mainPdfCount: number;
  /** 部门手册路径 */
  depPdfPath: string;
  /** 部门手册数量 */
  depPdfCount: number;
  /** 是否准备好了 */
  ready: boolean;
  constructor(public dirPath: string) {
    this.dirPath = dirPath;
    this.manualPlusDb = Path.resolve(dirPath, "ManualPlus.db3");
    this.manualDepDb = Path.resolve(dirPath, "DepartmentManual.db3");
    this.manualPlusDbExist = Fs.existsSync(this.manualPlusDb);
    this.manualDepDbExist = Fs.existsSync(this.manualDepDb);
    this.mainPdfPath = Path.resolve(dirPath, "Manual");
    // this.mainPdfPathDeep = Path.resolve(dirPath, "Manual/PDFFile");
    this.depPdfPath = Path.resolve(dirPath, "DepartmentManual");
    let mainPdfCount: number = 0;
    try {
      if (Fs.pathExistsSync(this.mainPdfPath)) {
        mainPdfCount = klawSync(this.mainPdfPath, { nodir: true }).filter((e) =>
          /.pdf$/.test(e.path)
        ).length;
      }
      this.mainPdfCount = mainPdfCount;
    } finally {
      this.mainPdfCount = mainPdfCount;
    }
    try {
      this.depPdfCount = klawSync(this.depPdfPath, {
        nodir: true,
        filter(e) {
          return Path.extname(e.path) === ".pdf";
        },
      }).length;
    } catch (error) {
      this.depPdfCount = 0;
    }
    this.ready =
      this.manualPlusDbExist &&
      this.manualDepDbExist &&
      this.mainPdfCount > 10 &&
      this.depPdfCount > 10;
  }
  removeEmptyDirs() {
    for (let { path } of klawSync(this.dirPath, { nofile: true })) {
      if (klawSync(path).length === 0) {
        Fs.removeSync(path);
      }
    }
  }
  start(
    callback: (
      /** 步骤 */
      step: number
    ) => void = () => ""
  ): number {
    let step = ref(0);
    // watchEffect(() => callback(step.value));
    let depGathered = Dep.toTask(this.manualDepDb);
    step.value++; // 1
    callback(step.value);
    Dep.exec(this.depPdfPath, depGathered);
    step.value++; //
    callback(step.value);
    let mainGatherd = Main.gather(this.manualPlusDb);
    step.value++; // 3
    callback(step.value);
    // Main.exec(this.mainPdfPathDeep, mainGatherd);
    Main.exec(this.mainPdfPath, mainGatherd);
    step.value++; // 4
    callback(step.value);
    this.removeEmptyDirs();
    open(this.dirPath);
    return step.value;
  }
}

export class TaskEnhuanced {
  task: Task;
  constructor(public dirPath: string) {
    this.task = new Task(dirPath);
  }
  refresh(dirPath?: string) {
    if (dirPath) {
      this.dirPath = dirPath;
    }
    // @ts-ignore
    this.task = null;
    this.task = new Task(dirPath || this.task.dirPath);
  }
}

export interface ProcessReq {
  dirPath: string;
  id: string;
}

export interface ProcessMsg {
  id: string;
  step: number;
}

process.on("message", ({ dirPath, id }: ProcessReq) => {
  let task = new Task(dirPath);
  if (task.ready) {
    task.start((step) => {
      // @ts-ignore
      process.send({ id, step } as ProcessMsg);
    });
  }
});

export function start(dirPath: string, cb: (step: number) => void) {
  let child = CP.fork(__filename);
  return new Promise((resolve) => {
    child.on("message", (msg: ProcessMsg) => {
      cb(msg.step);
      if (msg.step >= 4) {
        child.kill();
        resolve(1);
      }
    });
    let toSend: ProcessReq = { dirPath, id: nanoid() };
    child.send(toSend);
  });
}

export default TaskEnhuanced;
