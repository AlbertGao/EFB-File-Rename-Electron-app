export default async function () {
  const [{ startCountDown, dir, runScript, sleep }, Path, { ElMessage }] = [
    require("albert.gao"),
    require("path"),
    require("element-plus"),
  ];
  await startCountDown({
    countDownMessage: (time: number) => `${~~(time / 1000)}后将开始Python测试`,
  }).promise;
  console.log(
    await runScript({
      filePath: Path.join(__dirname, "../scripts/python-test/test1.py"),
      togetherWith: { files: ["env1.py"] },
      msgTo: ["Albert"],
      atDir: dir(["data", "python-test"]),
    })
  );
  await sleep(150);
  ElMessage({
    type: "success",
    message: "Python 文件运行成功，请查看console",
  });
  await startCountDown({
    countDownMessage: (time: number) => `${~~(time / 1000)}后将开始NodeJs测试`,
  }).promise;
  console.log(
    await runScript({
      filePath: Path.join(__dirname, "../scripts/Js-test/test1.js"),
      togetherWith: { files: ["env1.js"] },
      msgTo: ["Albert"],
      atDir: dir(["data", "NodeJs-test"]),
    })
  );
  await sleep(150);
  ElMessage({
    type: "success",
    message: "Js 文件运行成功，请查看console",
  });
}
