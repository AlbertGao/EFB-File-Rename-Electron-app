import { getModules, injectCss, showUrl, onPreload } from "albert.gao";
onPreload(() => {
  injectCss({ href: "//unpkg.com/element-plus/dist/index.css" });
  showUrl();
  getModules(__dirname).forEach((e) => {
    if (!/index.js$/.test(e.path)) {
      e.module.default();
    }
  });
});
