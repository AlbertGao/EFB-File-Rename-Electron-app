"子进程：清空某个目录的所有文件和文件夹";
import Fs from "fs-extra";
import process from "process";
process.on("message", ({ path }) => {
  Fs.emptyDirSync(path);
  // @ts-ignore
  process.send({ type: "success" });
});
