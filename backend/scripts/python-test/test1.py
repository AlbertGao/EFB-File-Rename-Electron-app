import sys
import json
from env1 import test2
msgTo = json.loads(sys.stdin.read())
print(json.dumps({
    "env1.py": test2(),
    "test1.py": __file__,
    "msgReceived": msgTo
}))
