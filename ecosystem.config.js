const Path = require("path");
module.exports = {
  apps: [
    {
      name: "Vue-Express-Server",
      script: "backend.js/index.js",
      args: "-production -ports=[80,8081]",
      instances: "1",
      output: Path.resolve(__dirname, "/logs/out.log"),
      error: Path.resolve(__dirname, "/logs/error.log"),
      merge_logs: true,
      // mode: "cluster",
    },
  ],
};
