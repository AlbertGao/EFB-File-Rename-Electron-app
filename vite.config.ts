import { defaultConfig } from "albert.gao/default/vite-config";
import { defineConfig } from "vite";
export default defineConfig({
  ...defaultConfig,
});
